import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { UserService } from './services/user.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor (private userSrv: UserService) {}
  
  canActivate () {
    if (!this.userSrv.user.logged) {
      alert('Activation blocked');
      return false;
    }

    return true;
  }

}