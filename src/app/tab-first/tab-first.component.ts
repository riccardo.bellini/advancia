import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import { FormService } from '../form.service';

@Component({
  selector: 'app-tab-first',
  templateUrl: './tab-first.component.html',
  styleUrls: ['./tab-first.component.scss']
})


export class TabFirstComponent implements OnInit {
  firstForm: FormGroup;
  email: FormControl;

  constructor (builder: FormBuilder, private formService:FormService) {
    this.email = new FormControl('', [Validators.required, Validators.minLength(5)]);
    this.firstForm = builder.group({
      email: this.email
    });
  }

  ngOnInit() {
  }

  firstSubmit () {
    this.formService.information.email = this.firstForm.value.email;
 }

}
