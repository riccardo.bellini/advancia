import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WrapperoneComponent } from './wrapperone/wrapperone.component';
import { QuotationListComponent } from './quotation-list/quotation-list.component';
import { TabFirstComponent } from './tab-first/tab-first.component';
import { TabSecondComponent } from './tab-second/tab-second.component';
import { LoginComponent } from './login/login.component';
import { LoginGuard } from './login.guard';

const routes: Routes = [
	{ path: '', redirectTo: 'preventivo', pathMatch: 'full'},
	{
		path: 'quotation-list', component: QuotationListComponent,
		canActivate : [LoginGuard]
	},
	{ path: 'preventivo', component: WrapperoneComponent,
	 	children : [
			{ path: '', redirectTo: 'tab-first', pathMatch: 'full'},
	 		{ path: 'tab-first', component: TabFirstComponent},
	 		{ path: 'tab-second', component: TabSecondComponent}
	 	]
	 },
	{ path: 'login', component: LoginComponent	}
];

@NgModule({
	imports : [RouterModule.forRoot(routes)],
	exports : [RouterModule],
	providers : []
})
export class RoutingModule{}
