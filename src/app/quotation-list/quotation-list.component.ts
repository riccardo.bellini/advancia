import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quotation-list',
  templateUrl: './quotation-list.component.html',
  styleUrls: ['./quotation-list.component.scss']
})
export class QuotationListComponent implements OnInit {

	quotations: any[] = [
     { 'num': '123', 'veicType': 'fsfsf', 'saleForce': 'ewew', 'ctrType': 'yyyy', 'finType': 'aaaa', 'matType': 'ffff' },
     { 'num': '456', 'veicType': 'ghghg', 'saleForce': 'rere', 'ctrType': 'uuuu', 'finType': 'ssss', 'matType': 'gggg' },
     { 'num': '789', 'veicType': 'ytyty', 'saleForce': 'trtr', 'ctrType': 'iiii', 'finType': 'dddd', 'matType': 'hhhh' }
	];

  constructor() { }

  ngOnInit() {
  }

}
